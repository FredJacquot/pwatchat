<?php include("header.php"); ?>
<div id="status" class="ONLINE"><p id="state">ONLINE</p></div>
<div id="log">Event: load; status=ONLINE</div>
<div class="row">
  <div class="col col-12 col-sm-12 col-md-6 col-lg-6">
    <form class="form-horizontal" method="post" action="">
      <fieldset>    
        <!-- pseudo-->
        <div class="form-group">
          <label class="col col-6 col-sm-6 col-md-4 control-label" for="pseudo" >Pseudo</label>  
          <div class="col-6 col-sm-6 col-md-4">
          <input id="pseudo" name="pseudo" type="text" placeholder="Pseudo (10 caractères max)" class="form-control input-md" maxlength="10" value="">
          </div>
          <div class="col col-6 col-sm-6 col-md-4">
            <button id="effacerPseudo" name="effacerPseudo" class="btn btn-secondary">Effacer le pseudo</button>
          </div>
        </div>
        
        <!-- message -->
        <div class="form-group">
          <label class="col col-6 col-sm-6 col-md-4 control-label" for="message">Message</label>
          <div class="col col-6 col-sm-6 col-md-4">                     
          <textarea placeholder="(255 caractères max)" maxlength="255" class="form-control" id="message" name="message"></textarea>
          </div>
        </div>
        
        <!-- Button -->
        <div class="form-group">
          <label class="col col-6 col-sm-6 col-md-4 control-label" for="submit"></label>
          <div class="col col-6 col-sm-6 col-md-4">
          <button id="submit" name="submit" class="btn btn-default">Envoyer</button>
          </div>
        </div>
      </fieldset>

    </form>
  </div>
  <div class="col col-12 col-sm-12 col-md-6 col-lg-6">
    <div id="tchat"><?php include("affichage_chat.php"); ?></div>
  </div>
</div>

<!-- Script Bootstrap -->
<script src="assets/js/jquery-3.1.1.js"></script>
<script src="assets/js/bootstrap.js"></script>


<script>
  /* Script SW PWA */
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('sw.js')
    .then(() => navigator.serviceWorker.ready)
    .then(registration => {
      if ('SyncManager' in window) {
        registration.sync.register('sync-messages');
      }
    })
  }

  /* Mise en localstorage message et pseudo */
  
  let pseudo=document.getElementById('pseudo');
  let effacerPseudo = document.getElementById('effacerPseudo');
  let message=document.getElementById('message');
  let submit=document.getElementById('submit');
  
  effacerPseudo.addEventListener('click', ()=> {
    effacerPseudo.value = '';
  })
  submit.addEventListener('click', ()=>{
    event.preventDefault();

    if(pseudo.value=="" || message.value=="") {                     // on vérifie que le pseudo et le message soient complétés
      alert('Il faut entrer un pseudo et un message!')              // sinon on affiche une alerte (alerte à revoir plus tard)
    } else {                                                        // si c'est bon on passe à la suite
          if(navigator.onLine) {   
          let envoi={                                               // on récupère les données du formulaire
          'pseudo':pseudo.value, 
          'message':message.value
          };                                                         // si le navigateur est en ligne
          console.log(envoi);
          $.ajax({                                                   // on crée une requete AJAX pour envoyer les données à la page PHP qui va traiter le formulaire
          type: "POST",
          url: "minichat_post.php",         
          data: envoi,
          success: function(envoi) {
            console.log("Base mise à jour");
            refreshTchat();
            console.log(envoi);
            displayNotifications(pseudo.value,message.value);
            }
          })
        } else {                                                     // sinon, si le navigateur n'est pas en ligne
          localStorage.setItem('pseudo', pseudo.value);              // on stocke le pseudo et le message dans le localstorage
          localStorage.setItem('message', message.value);
                                                                     // on bloque ensuite l'input pseudo, message et le bouton envoyer
          }
        } 
      }
    )

function refreshTchat(){
  $.ajax({
    type: "POST",
    url: 'affichage_chat.php',
    dataType: 'html',
    success: function(response) {
      $('#tchat').html(response);
      }
    })
  }



  /* Notifications */

  if(Notification.permission==="granted"){
    displayNotifications('Aministrateur', 'Merci de votre confiance :)');
  }else if(Notification.permission !=="denied"){
    Notification.requestPermission().then(permission => {
      console.log(permission)
    })
  }

  function displayNotifications(pseudo, message){
    title="Nouveau message de : " + pseudo;
      let params={
        body: message,
        icon:'assets/images/android-favicon-192x192.png'
      }
  
    const notifications= new Notification (title, params)
  }





</script>
</body>
</html>