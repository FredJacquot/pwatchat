    var cacheName = 'lpdwca-PWATCHAT-v3';
    var appShellFiles = [
        'index.php',
        'header.php',
        'affichage_chat.php',
        'connexionDB.php',
        'minichat_post.php',
        'sw.js',
        'assets/js/jquery-3.1.1.js',
        'assets/js/npm.js',
        'assets/js/bootstrap.min.js',
        'assets/css/stylePerso.css',
        'assets/css/bootstrap.min.css',
        'assets/css/bootstrap-theme.min.css'
      ];
    

    self.addEventListener('install', (e) => {
        console.log('[Service Worker] Installation');
        e.waitUntil(
          caches.open(cacheName).then((cache) => {
                console.log('[Service Worker] Mise en cache globale: app shell et contenu');
            return cache.addAll(appShellFiles);
          })
        );
      });
      


    self.addEventListener('fetch', (e) => {
        e.respondWith(
          caches.match(e.request).then((r) => {
                console.log('[Service Worker] Récupération de la ressource: '+e.request.url);
                return r || fetch(e.request).then((response) => {
                      return caches.open(cacheName).then((cache) => {
                console.log('[Service Worker] Mise en cache de la nouvelle ressource: '+e.request.url);
                cache.put(e.request, response.clone());
                return response;
              });
            });
          })
        );
        if (!navigator.onLine){
          evt.respondWith( new Response('pas de connexion     internet'))
          }
          if(localStorage.getItem('pseudo')=="" || localStorage.getItem('message')){
            var pseudo="";
            var message="";
          } else {
            var pseudo=   localStorage.getItem('pseudo');
            var message= localStorage.getItem('message');
          }
          document.getElementById('pseudo')=pseudo + 'mis à jour :)';              // on stocke le pseudo et le message dans le localstorage
          document.getElementById('message')=message + 'mis à jour :)';
          localStorage.clear
      });


    self.addEventListener('activate', (e) => {
    e.waitUntil(
        caches.keys().then((keyList) => {
            return Promise.all(keyList.map((key) => {
            if(cacheName.indexOf(key) === -1) {
            return caches.delete(key);
            }
        }));
        })
      );
    });

    self.addEventListener("notificationclick", (event) => {
      const pushData = event.notification.data;
      event.notification.close();   
      self.clients.openWindow('www.google.fr')
   });




/* 
    self.addEventListener('online', evt=> {
      let messages=[];                                              // on crée le tableau messages qui contiendra les messages
      if(localStorage.getItem('messages')){                         // s'il y a déjà des messages dans le localstorage
        messages=JSON.parse(localStorage.getItem('messages'));      // on alimente le tableau avec
      }
      messages.forEach(message => {
        console.log(message);
      });
    })  */


    self.addEventListener("load", () => {
      function handleNetworkChange(event) {
        if (navigator.onLine) {
          //document.body.classList.remove("offline");
          if(localStorage.getItem('pseudo')=="" || localStorage.getItem('message')){
            var pseudo="";
            var message="";
          } else {
            var pseudo=   localStorage.getItem('pseudo');
            var message= localStorage.getItem('message');
          }
          console.log()
          document.getElementById('pseudo')=pseudo;              // on stocke le pseudo et le message dans le localstorage
          document.getElementById('message')=message;
          localStorage.clear
        } else {
          //document.body.classList.add("offline");
        }
      }
      self.addEventListener("online", handleNetworkChange);
      self.addEventListener("offline", handleNetworkChange);
    });
  
