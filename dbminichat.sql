-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 18 avr. 2021 à 09:36
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `dbminichat`
--

-- --------------------------------------------------------

--
-- Structure de la table `minichat`
--

DROP TABLE IF EXISTS `minichat`;
CREATE TABLE IF NOT EXISTS `minichat` (
  `id_message` int(100) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(10) CHARACTER SET latin1 NOT NULL,
  `message` text CHARACTER SET latin1 NOT NULL,
  `date_message` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_message`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Déchargement des données de la table `minichat`
--

INSERT INTO `minichat` (`id_message`, `pseudo`, `message`, `date_message`) VALUES
(1, 'Fred', 'Bienvenu dans le chat :)', '2017-03-04 12:12:06'),
(2, 'Clark Kent', 'Hey, c\'est chouette ici !', '2017-03-04 12:12:25'),
(3, 'Fred', 'N\'est-ce pas? ', '2017-03-04 12:21:46'),
(4, 'Clark Kent', 'Par contre, la mise en page déconne un peu non?', '2017-03-04 12:22:07'),
(5, 'Fred', 'Oui, faut que j\'améliore cela...', '2017-03-04 12:22:26'),
(6, 'Clark Kent', 'Ah, c\'est carrément mieux comme ça :)', '2017-03-04 12:25:49'),
(7, 'Fred', 'Et oui. Je te laisse, je file manger :P', '2017-03-04 12:26:37'),
(8, 'Clark Kent', 'Bon ap\', j\'y go aussi', '2017-03-04 12:26:46'),
(9, 'Fred', 'Merci', '2017-03-04 12:26:53'),
(10, 'Fred', 'A toi aussi', '2017-03-04 12:27:11'),
(11, 'Fred', 'je teste les cookies', '2017-03-04 14:49:55'),
(12, 'Clark Kent', 'Alors ça donne quoi?', '2017-03-04 15:37:28'),
(13, 'Fred', 'C\'est pas trop concluant pour le moment', '2017-03-04 15:42:40'),
(14, 'Clark Kent', 'Et maintenant?', '2017-03-04 15:45:58'),
(15, 'Fred', 'Ca retient bien le pseudo mais y\'a des erreurs quand le pseudo n\'est pas défini', '2017-03-04 15:46:32'),
(16, 'Fred', 'C\'est bon ça marche =D', '2017-03-04 15:49:03'),
(17, 'Fred', 'Y\'a plus qu\'à travailler sur la pagination maintnenant', '2017-03-04 15:49:30'),
(18, 'Tom', 'Salut tout le monde! Alors ça marche ce chat?', '2017-03-04 16:24:51'),
(19, 'Fred', 'Salut! Oui, ça me semble pas mal :)', '2017-03-04 16:25:10'),
(20, 'Clark Kent', 'Salut Tom! Comment va?', '2017-03-04 16:26:13'),
(21, 'Hackerman', '&lt;script&gt;alert(\'Il y a une faille XSS\')&lt;/script&gt;', '2017-03-04 16:59:47'),
(22, 'Tom', 'Haha! Et bien non, pas de soucis de ce côté là', '2017-03-04 17:00:14'),
(23, 'Hackerman', 'Je suis très déçu :(', '2017-03-04 17:00:25'),
(24, 'Fred', 'Tout maaaaarche!!! Youhouuuuuuuuu', '2017-03-04 21:28:40'),
(25, 'Fred', 'Salut tout le monde, ça fait un bail!', '2019-03-10 12:23:03'),
(26, 'Fred', 'test pagination bis', '2019-03-10 12:31:31'),
(27, 'test', 'fsdgfsdg', '2019-03-14 07:03:12'),
(28, 'test', 'test 2', '2019-03-14 07:03:54'),
(29, 'fredo', 'coucou', '2019-03-14 20:06:07'),
(30, 'Fred', 'fsdsdg', '2021-04-13 12:30:36'),
(31, 'Fred', 'Test', '2021-04-13 13:59:39'),
(32, 'Fred', 'test encore', '2021-04-13 13:59:59'),
(33, 'Fred', 'rett', '2021-04-13 14:03:50'),
(34, 'Fred', 'Teste mode enligne', '2021-04-13 14:04:10'),
(35, 'Fred', 'dsfdsfsdf', '2021-04-13 14:07:51'),
(36, 'Fred', 'gddgdgdfg', '2021-04-13 14:10:08'),
(37, 'Fred', 'gddgdgdfg', '2021-04-13 14:11:26'),
(38, 'Fred', 'vfdgdgdfgdfg', '2021-04-13 14:11:32'),
(39, 'Fred', 'fdsfsdfsf', '2021-04-13 14:11:45'),
(40, 'Frefd', 'fsdfdsfsd', '2021-04-13 14:13:03'),
(41, 'Frefd', 'fsdfdsfsd', '2021-04-13 14:13:07'),
(42, 'Fred', 'test refresh', '2021-04-13 14:19:37'),
(43, 'Fred', 'test refresh 2', '2021-04-13 14:20:03'),
(44, 'Fred', 'refds', '2021-04-13 14:23:06'),
(45, 'Fred', 'sfsdfsdfsdfsdqfSFSDF', '2021-04-13 14:23:13'),
(46, 'Fred', 'SDFDSFSDFSDF', '2021-04-13 14:23:20'),
(47, 'Fred', 'SDFDSFSDF', '2021-04-13 14:23:23'),
(48, 'Fred', 'fdsfsd', '2021-04-13 14:25:33'),
(49, 'Fred', 'test refresh', '2021-04-13 14:25:46'),
(50, 'Fred', 'gfgtdg', '2021-04-13 22:15:01'),
(51, 'Fred', 'gdfgdgdhgdg', '2021-04-13 22:15:52'),
(52, 'Fred', 'test', '2021-04-13 22:16:37'),
(53, 'Fred', 'gfdgdfg', '2021-04-13 22:17:22'),
(54, 'Fred', 'gdfgdjgh,,kiulul', '2021-04-13 22:17:31'),
(55, 'sdfsdfsdfs', 'sdfsdfsdf', '2021-04-13 23:14:31'),
(56, 'uytu', 'utyu', '2021-04-14 01:59:51'),
(57, 'utyu', 'tyuytu', '2021-04-14 01:59:57'),
(58, 'fdsf', 'dsfsdf', '2021-04-14 02:07:45'),
(59, 'yutu', 'yutyu', '2021-04-14 02:12:39'),
(60, 'dfgg', 'dfgdfg', '2021-04-14 02:13:27'),
(61, 'gdfgdfg', 'gdgdf', '2021-04-14 02:27:46'),
(62, 'yuytu', 'tjytgjtj', '2021-04-14 02:27:56'),
(63, 'Fred', 'il est tard', '2021-04-14 02:42:20'),
(64, 'Fred', 'fdsffs', '2021-04-14 02:45:11'),
(65, 'fzfsdf', 'fsdfsdf', '2021-04-14 02:49:19'),
(66, 'gdfg', 'dgfgdf', '2021-04-14 03:00:11'),
(67, 'Tedqsd', 'sdffsdf', '2021-04-14 03:01:21'),
(68, 'gfgd', 'gdfg', '2021-04-14 03:04:55'),
(69, 'dfgdf', 'ggdfg', '2021-04-14 03:04:59'),
(70, 'gdfg', 'dgdfg', '2021-04-14 03:06:28'),
(71, 'dfgdf', 'gdfgdg', '2021-04-14 03:06:33'),
(72, 'hghfh', 'hfghfhf', '2021-04-14 03:09:41'),
(73, 'hghfh', 'hfghfhf', '2021-04-14 03:09:44'),
(74, 'hghfh', 'hfghfhf', '2021-04-14 03:09:46'),
(75, 'Test', 'refresf tchat', '2021-04-15 05:49:39'),
(76, 'test', 'fdsfsd', '2021-04-15 05:51:40'),
(77, 'test', 'fdsfsd', '2021-04-15 05:51:41'),
(78, 'test', 'fdsfsd', '2021-04-15 05:51:42'),
(79, 'test', 'fdsfsd', '2021-04-15 05:51:42'),
(80, 'test', 'fdsfsd', '2021-04-15 05:51:42'),
(81, 'test', 'fdsfsd', '2021-04-15 05:51:42'),
(82, 'test', 'fdsfsd', '2021-04-15 05:51:42'),
(83, 'test', 'fdsfsd', '2021-04-15 05:51:43'),
(84, 'test', 'fdsfsd', '2021-04-15 05:51:43'),
(85, 'test', 'fdsfsd', '2021-04-15 05:51:43'),
(86, 'test', 'fdsfsd', '2021-04-15 05:51:43'),
(87, 'test', 'fdsfsd', '2021-04-15 05:51:43'),
(88, 'test', 'fdsfsd', '2021-04-15 05:51:44'),
(89, 'test', 'fdsfsd', '2021-04-15 05:51:44'),
(90, 'test', 'fdsfsd', '2021-04-15 05:51:44'),
(91, 'test', 'fdsfsd', '2021-04-15 05:51:44'),
(92, 'test', 'fdsfsd', '2021-04-15 05:51:44'),
(93, 'Tedsqdq', 'dqqsqsqsqssq', '2021-04-15 05:52:22'),
(94, 'Tedsqdq', 'dqqsqsqsqssq', '2021-04-15 05:52:24'),
(95, 'fdsfdsf', 'fsdfsdf', '2021-04-15 05:52:30'),
(96, 'fdsfdsf', 'fsdfsdf', '2021-04-15 05:52:33'),
(97, 'fdsfdsf', 'fsdfsdf', '2021-04-15 05:52:34'),
(98, 'fsfds', 'fsdfsd', '2021-04-15 05:53:24'),
(99, 'fsfds', 'fsdfsd', '2021-04-15 05:53:25'),
(100, 'fsfds', 'fsdfsd', '2021-04-15 05:53:26'),
(101, 'fsfds', 'fsdfsd', '2021-04-15 05:53:26'),
(102, 'fsfds', 'fsdfsd', '2021-04-15 05:53:26'),
(103, 'fsfds', 'fsdfsdfdsfdsfdsfsdfdsfdsfdsfdsfdssfds', '2021-04-15 05:53:30'),
(104, 'fsfds', 'fsdfsdfdsfdsfdsfsdfdsfdsfdsfdsfdssfds', '2021-04-15 05:53:31'),
(105, 'fsfds', 'fsdfsdfdsfdsfdsfsdfdsfdsfdsfdsfdssfds', '2021-04-15 05:53:31'),
(106, 'fsfds', 'fsdfsdfdsfdsfdsfsdfdsfdsfdsfdsfdssfds', '2021-04-15 05:53:31'),
(107, 'fsfds', 'fsdfsdfdsfdsfdsfsdfdsfdsfdsfdsfdssfds', '2021-04-15 05:53:31'),
(108, 'fsfds', 'fsdfsdfdsfdsfdsfsdfdsfdsfdsfdsfdssfds', '2021-04-15 05:53:31'),
(109, 'fsfds', 'fsdfsdfdsfdsfdsfsdfdsfdsfdsfdsfdssfds', '2021-04-15 05:53:32'),
(110, 'fsfds', 'fsdfsdfdsfdsfdsfsdfdsfdsfdsfdsfdssfds', '2021-04-15 05:53:32'),
(111, 'fsfds', 'fsdfsdfdsfdsfdsfsdfdsfdsfdsfdsfdssfds', '2021-04-15 05:53:32'),
(112, 'fsfds', 'fsdfsdfdsfdsfdsfsdfdsfdsfdsfdsfdssfds', '2021-04-15 05:53:33'),
(113, 'fdsfd', 'fsdf', '2021-04-15 05:57:18'),
(114, 'test', 'Salut les copains\n', '2021-04-15 06:07:16'),
(115, 'test', 'Salut les copains\n', '2021-04-15 06:08:19'),
(116, 'test', 'Salut les copains\n', '2021-04-15 06:08:20'),
(117, 'rsr', 'fsd', '2021-04-15 06:08:35'),
(118, 'test', 'fsdfsd', '2021-04-15 06:17:14'),
(119, 'Test', 'Fdfsdfsdf', '2021-04-18 11:27:44');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
