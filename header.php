<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Tchat Ben & Fred</title>
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link type="text/css" rel="stylesheet" href="assets/css/stylePerso.css">

      <!-- Meta tags pour PWA -->
  <link rel="canonical" href="https://tchatfred.alwaysdata.net/"/>
  <meta name="theme-color" content="#c4c4c4">
  <link rel="apple-touch-icon" sizes="57x57" href="assets/images/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="assets/images/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="assets/images/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="assets/images/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="assets/images/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192" href="assets/images/android-favicon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png">
  <link rel = "manifest" href = "assets/js/manifest.json">

</head>
<body onload='refreshTchat()'>
<header class="page-header">       
  <div class="row">
    <div class="col col-12 col-sm-12 col-md-6 col-lg-6 text-center">
        <h1>Minichat</h1>
    </div>
    <div class="col col-12 col-sm-12 col-md-6 col-lg-6 text-center">
      <h1>Messages</h1>
    </div>
  </div>
</header>

