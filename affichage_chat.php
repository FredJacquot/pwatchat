<div id="tchat" class="col-md-12 col-lg-12">
  <table class="table table-striped table-condensed">
    <thead>
     <tr>
       <th class="col col-4 col-sm-4 col-md-4">Date</th>
       <th class="col col-2 col-sm-2 col-md-2">Pseudo</th>
       <th class="col col-6 col-sm-6 col-md-6">Message</th>
     </tr>
    </thead>
    <tbody>
    <?php
    include('connexionDB.php');

      // Récupération des 10 derniers messages
      $reponse = $bdd->query('SELECT pseudo, message, date_message FROM minichat ORDER BY id_message DESC');

      // Affichage de chaque message (toutes les données sont protégées par htmlspecialchars)
      while ($donnees = $reponse->fetch(PDO::FETCH_ASSOC))
      {
        $date = new DateTime(htmlspecialchars($donnees['date_message']));
      
        echo '<tr>';
        echo '<td class="col-md-4"> Le ' . $date->format('d/m/Y') . ' à ' . $date->format('H:i:s') . '</td>';
        echo '<td class="col-md-2">' . htmlspecialchars($donnees['pseudo']) . '</td>';
        echo '<td class="col-md-6">' . htmlspecialchars($donnees['message']) . '</td>';
        echo '</tr>';
      }
      $reponse->closeCursor();
    ?>
    </tbody>
  </table>
</div>
