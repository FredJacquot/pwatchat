<?php
include('connexionDB.php');
// Si le pseudo ou le message est bien rempli
 if (!empty($_POST['pseudo']) AND !empty($_POST['message'])) 
{
    $PSEUDO = htmlspecialchars($_POST['pseudo']);
    $MESSAGE = htmlspecialchars($_POST['message']);
    $req = $bdd->prepare('INSERT INTO minichat (pseudo, message) VALUES (:pseudo, :message)');
    $req ->execute(array(
        'pseudo'=>$PSEUDO,
        'message'=>$MESSAGE
    ));
    header('Location: index.php'); 
}
// sinon message d'erreur
else
{
  echo "Probleme données DATA";
}
?>
<div>
  <a href="index.php">Retour</a>
</div>
